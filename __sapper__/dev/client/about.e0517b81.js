import { S as SvelteComponentDev, i as init, s as safe_not_equal, d as dispatch_dev, v as validate_slots, f as space, c as element, C as text, B as svg_element, j as create_component, q as query_selector_all, b as detach_dev, m as claim_space, k as claim_element, l as children, D as claim_text, n as claim_component, o as attr_dev, p as add_location, a as insert_dev, t as append_dev, w as mount_component, E as noop, y as transition_in, z as transition_out, A as destroy_component } from './client.ac2285fe.js';
import { F as Footer } from './Footer.2d9c950a.js';

/* src/routes/about.svelte generated by Svelte v3.31.2 */
const file = "src/routes/about.svelte";

function create_fragment(ctx) {
	let t0;
	let div7;
	let div6;
	let img;
	let img_src_value;
	let t1;
	let div5;
	let div0;
	let h2;
	let t2;
	let t3;
	let small;
	let t4;
	let t5;
	let p0;
	let t6;
	let t7;
	let p1;
	let t8;
	let t9;
	let p2;
	let a0;
	let t10;
	let t11;
	let t12;
	let p3;
	let t13;
	let a1;
	let t14;
	let t15;
	let t16;
	let div4;
	let div1;
	let svg0;
	let path0;
	let t17;
	let span0;
	let t18;
	let t19;
	let div2;
	let svg1;
	let path1;
	let t20;
	let span1;
	let t21;
	let t22;
	let div3;
	let svg2;
	let path2;
	let t23;
	let span2;
	let t24;
	let t25;
	let footer;
	let current;
	footer = new Footer({ $$inline: true });

	const block = {
		c: function create() {
			t0 = space();
			div7 = element("div");
			div6 = element("div");
			img = element("img");
			t1 = space();
			div5 = element("div");
			div0 = element("div");
			h2 = element("h2");
			t2 = text("Sheila");
			t3 = space();
			small = element("small");
			t4 = text("Open Source for Libraries");
			t5 = space();
			p0 = element("p");
			t6 = text("Subject HEadIng for better Library Application");
			t7 = space();
			p1 = element("p");
			t8 = text("Sheila merupakan akronim dari Subject HEadIng for better Library Application. Merupakan inisiatif agar layanan Tajuk Online (https://tajukonline.perpusnas.go.id) \n\t\t\t\t dari Perpustakaan Nasional bisa digunakan oleh (utamanya) para developer aplikasi bidang Pusdokinfo (Perpustakaan, Dokumentasi, Kearsipan dan Informasi).");
			t9 = space();
			p2 = element("p");
			a0 = element("a");
			t10 = text("Komponen utama Sheila");
			t11 = text(" adalah backend yang fungsinya melakukan web-scraping luaran format JSON https://tajukonline.perpusnas.go.id berdasarkan kata kunci \n         atau detail tajuk tertentu. of a JSON output. Sehingga bisa digunakan oleh aplikasi secara langsung.");
			t12 = space();
			p3 = element("p");
			t13 = text("Komponen berikutnya ada ");
			a1 = element("a");
			t14 = text("sampel aplikasi frontend");
			t15 = text(" pencarian tajuk online menggunakan backend Sheila.");
			t16 = space();
			div4 = element("div");
			div1 = element("div");
			svg0 = svg_element("svg");
			path0 = svg_element("path");
			t17 = space();
			span0 = element("span");
			t18 = text("12");
			t19 = space();
			div2 = element("div");
			svg1 = svg_element("svg");
			path1 = svg_element("path");
			t20 = space();
			span1 = element("span");
			t21 = text("8");
			t22 = space();
			div3 = element("div");
			svg2 = svg_element("svg");
			path2 = svg_element("path");
			t23 = space();
			span2 = element("span");
			t24 = text("share");
			t25 = space();
			create_component(footer.$$.fragment);
			this.h();
		},
		l: function claim(nodes) {
			const head_nodes = query_selector_all("[data-svelte=\"svelte-bqrti\"]", document.head);
			head_nodes.forEach(detach_dev);
			t0 = claim_space(nodes);
			div7 = claim_element(nodes, "DIV", { class: true });
			var div7_nodes = children(div7);
			div6 = claim_element(div7_nodes, "DIV", { class: true });
			var div6_nodes = children(div6);
			img = claim_element(div6_nodes, "IMG", { class: true, src: true, alt: true });
			t1 = claim_space(div6_nodes);
			div5 = claim_element(div6_nodes, "DIV", { class: true });
			var div5_nodes = children(div5);
			div0 = claim_element(div5_nodes, "DIV", { class: true });
			var div0_nodes = children(div0);
			h2 = claim_element(div0_nodes, "H2", { class: true });
			var h2_nodes = children(h2);
			t2 = claim_text(h2_nodes, "Sheila");
			h2_nodes.forEach(detach_dev);
			t3 = claim_space(div0_nodes);
			small = claim_element(div0_nodes, "SMALL", { class: true });
			var small_nodes = children(small);
			t4 = claim_text(small_nodes, "Open Source for Libraries");
			small_nodes.forEach(detach_dev);
			div0_nodes.forEach(detach_dev);
			t5 = claim_space(div5_nodes);
			p0 = claim_element(div5_nodes, "P", { class: true });
			var p0_nodes = children(p0);
			t6 = claim_text(p0_nodes, "Subject HEadIng for better Library Application");
			p0_nodes.forEach(detach_dev);
			t7 = claim_space(div5_nodes);
			p1 = claim_element(div5_nodes, "P", { class: true });
			var p1_nodes = children(p1);
			t8 = claim_text(p1_nodes, "Sheila merupakan akronim dari Subject HEadIng for better Library Application. Merupakan inisiatif agar layanan Tajuk Online (https://tajukonline.perpusnas.go.id) \n\t\t\t\t dari Perpustakaan Nasional bisa digunakan oleh (utamanya) para developer aplikasi bidang Pusdokinfo (Perpustakaan, Dokumentasi, Kearsipan dan Informasi).");
			p1_nodes.forEach(detach_dev);
			t9 = claim_space(div5_nodes);
			p2 = claim_element(div5_nodes, "P", { class: true });
			var p2_nodes = children(p2);
			a0 = claim_element(p2_nodes, "A", { href: true, class: true });
			var a0_nodes = children(a0);
			t10 = claim_text(a0_nodes, "Komponen utama Sheila");
			a0_nodes.forEach(detach_dev);
			t11 = claim_text(p2_nodes, " adalah backend yang fungsinya melakukan web-scraping luaran format JSON https://tajukonline.perpusnas.go.id berdasarkan kata kunci \n         atau detail tajuk tertentu. of a JSON output. Sehingga bisa digunakan oleh aplikasi secara langsung.");
			p2_nodes.forEach(detach_dev);
			t12 = claim_space(div5_nodes);
			p3 = claim_element(div5_nodes, "P", { class: true });
			var p3_nodes = children(p3);
			t13 = claim_text(p3_nodes, "Komponen berikutnya ada ");
			a1 = claim_element(p3_nodes, "A", { href: true, class: true });
			var a1_nodes = children(a1);
			t14 = claim_text(a1_nodes, "sampel aplikasi frontend");
			a1_nodes.forEach(detach_dev);
			t15 = claim_text(p3_nodes, " pencarian tajuk online menggunakan backend Sheila.");
			p3_nodes.forEach(detach_dev);
			t16 = claim_space(div5_nodes);
			div4 = claim_element(div5_nodes, "DIV", { class: true });
			var div4_nodes = children(div4);
			div1 = claim_element(div4_nodes, "DIV", { class: true });
			var div1_nodes = children(div1);

			svg0 = claim_element(
				div1_nodes,
				"svg",
				{
					fill: true,
					viewBox: true,
					class: true,
					stroke: true
				},
				1
			);

			var svg0_nodes = children(svg0);

			path0 = claim_element(
				svg0_nodes,
				"path",
				{
					"stroke-linecap": true,
					"stroke-linejoin": true,
					"stroke-width": true,
					d: true
				},
				1
			);

			children(path0).forEach(detach_dev);
			svg0_nodes.forEach(detach_dev);
			t17 = claim_space(div1_nodes);
			span0 = claim_element(div1_nodes, "SPAN", {});
			var span0_nodes = children(span0);
			t18 = claim_text(span0_nodes, "12");
			span0_nodes.forEach(detach_dev);
			div1_nodes.forEach(detach_dev);
			t19 = claim_space(div4_nodes);
			div2 = claim_element(div4_nodes, "DIV", { class: true });
			var div2_nodes = children(div2);

			svg1 = claim_element(
				div2_nodes,
				"svg",
				{
					fill: true,
					viewBox: true,
					class: true,
					stroke: true
				},
				1
			);

			var svg1_nodes = children(svg1);

			path1 = claim_element(
				svg1_nodes,
				"path",
				{
					"stroke-linecap": true,
					"stroke-linejoin": true,
					"stroke-width": true,
					d: true
				},
				1
			);

			children(path1).forEach(detach_dev);
			svg1_nodes.forEach(detach_dev);
			t20 = claim_space(div2_nodes);
			span1 = claim_element(div2_nodes, "SPAN", {});
			var span1_nodes = children(span1);
			t21 = claim_text(span1_nodes, "8");
			span1_nodes.forEach(detach_dev);
			div2_nodes.forEach(detach_dev);
			t22 = claim_space(div4_nodes);
			div3 = claim_element(div4_nodes, "DIV", { class: true });
			var div3_nodes = children(div3);

			svg2 = claim_element(
				div3_nodes,
				"svg",
				{
					fill: true,
					viewBox: true,
					class: true,
					stroke: true
				},
				1
			);

			var svg2_nodes = children(svg2);

			path2 = claim_element(
				svg2_nodes,
				"path",
				{
					"stroke-linecap": true,
					"stroke-linejoin": true,
					"stroke-width": true,
					d: true
				},
				1
			);

			children(path2).forEach(detach_dev);
			svg2_nodes.forEach(detach_dev);
			t23 = claim_space(div3_nodes);
			span2 = claim_element(div3_nodes, "SPAN", {});
			var span2_nodes = children(span2);
			t24 = claim_text(span2_nodes, "share");
			span2_nodes.forEach(detach_dev);
			div3_nodes.forEach(detach_dev);
			div4_nodes.forEach(detach_dev);
			div5_nodes.forEach(detach_dev);
			div6_nodes.forEach(detach_dev);
			div7_nodes.forEach(detach_dev);
			t25 = claim_space(nodes);
			claim_component(footer.$$.fragment, nodes);
			this.h();
		},
		h: function hydrate() {
			document.title = "About Sheila";
			attr_dev(img, "class", "w-12 h-12 rounded-full object-cover mr-4 shadow");
			if (img.src !== (img_src_value = "https://robohash.org/sheila")) attr_dev(img, "src", img_src_value);
			attr_dev(img, "alt", "avatar");
			add_location(img, file, 10, 4, 322);
			attr_dev(h2, "class", "text-lg font-semibold text-gray-900 -mt-1");
			add_location(h2, file, 13, 12, 521);
			attr_dev(small, "class", "text-sm text-gray-700");
			add_location(small, file, 14, 12, 599);
			attr_dev(div0, "class", "flex items-center justify-between");
			add_location(div0, file, 12, 9, 461);
			attr_dev(p0, "class", "text-gray-700");
			add_location(p0, file, 16, 9, 695);
			attr_dev(p1, "class", "mt-3 text-gray-700 text-sm");
			add_location(p1, file, 17, 9, 780);
			attr_dev(a0, "href", "https://gitlab.com/sheila-subject-heading/backend-api");
			attr_dev(a0, "class", "underline");
			add_location(a0, file, 22, 9, 1221);
			attr_dev(p2, "class", "mt-3 text-gray-700 text-sm");
			add_location(p2, file, 21, 9, 1173);
			attr_dev(a1, "href", "https://gitlab.com/sheila-subject-heading/frontend-sample");
			attr_dev(a1, "class", "underline");
			add_location(a1, file, 26, 33, 1666);
			attr_dev(p3, "class", "mt-3 text-gray-700 text-sm");
			add_location(p3, file, 25, 9, 1594);
			attr_dev(path0, "stroke-linecap", "round");
			attr_dev(path0, "stroke-linejoin", "round");
			attr_dev(path0, "stroke-width", "2");
			attr_dev(path0, "d", "M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z");
			add_location(path0, file, 31, 18, 2070);
			attr_dev(svg0, "fill", "none");
			attr_dev(svg0, "viewBox", "0 0 24 24");
			attr_dev(svg0, "class", "w-4 h-4 mr-1");
			attr_dev(svg0, "stroke", "currentColor");
			add_location(svg0, file, 30, 15, 1970);
			add_location(span0, file, 33, 15, 2308);
			attr_dev(div1, "class", "flex mr-2 text-gray-700 text-sm mr-3");
			add_location(div1, file, 29, 12, 1904);
			attr_dev(path1, "stroke-linecap", "round");
			attr_dev(path1, "stroke-linejoin", "round");
			attr_dev(path1, "stroke-width", "2");
			attr_dev(path1, "d", "M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z");
			add_location(path1, file, 37, 18, 2520);
			attr_dev(svg1, "fill", "none");
			attr_dev(svg1, "viewBox", "0 0 24 24");
			attr_dev(svg1, "class", "w-4 h-4 mr-1");
			attr_dev(svg1, "stroke", "currentColor");
			add_location(svg1, file, 36, 15, 2421);
			add_location(span1, file, 39, 15, 2791);
			attr_dev(div2, "class", "flex mr-2 text-gray-700 text-sm mr-8");
			add_location(div2, file, 35, 12, 2355);
			attr_dev(path2, "stroke-linecap", "round");
			attr_dev(path2, "stroke-linejoin", "round");
			attr_dev(path2, "stroke-width", "2");
			attr_dev(path2, "d", "M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12");
			add_location(path2, file, 43, 18, 3002);
			attr_dev(svg2, "fill", "none");
			attr_dev(svg2, "viewBox", "0 0 24 24");
			attr_dev(svg2, "class", "w-4 h-4 mr-1");
			attr_dev(svg2, "stroke", "currentColor");
			add_location(svg2, file, 42, 15, 2903);
			add_location(span2, file, 45, 15, 3179);
			attr_dev(div3, "class", "flex mr-2 text-gray-700 text-sm mr-4");
			add_location(div3, file, 41, 12, 2837);
			attr_dev(div4, "class", "mt-4 flex items-center");
			add_location(div4, file, 28, 9, 1855);
			attr_dev(div5, "class", "");
			add_location(div5, file, 11, 6, 437);
			attr_dev(div6, "class", "flex items-start px-4 py-6");
			add_location(div6, file, 9, 3, 277);
			attr_dev(div7, "class", "flex bg-white shadow-lg rounded-lg mx-4 md:mx-auto my-3 container");
			add_location(div7, file, 8, 0, 150);
		},
		m: function mount(target, anchor) {
			insert_dev(target, t0, anchor);
			insert_dev(target, div7, anchor);
			append_dev(div7, div6);
			append_dev(div6, img);
			append_dev(div6, t1);
			append_dev(div6, div5);
			append_dev(div5, div0);
			append_dev(div0, h2);
			append_dev(h2, t2);
			append_dev(div0, t3);
			append_dev(div0, small);
			append_dev(small, t4);
			append_dev(div5, t5);
			append_dev(div5, p0);
			append_dev(p0, t6);
			append_dev(div5, t7);
			append_dev(div5, p1);
			append_dev(p1, t8);
			append_dev(div5, t9);
			append_dev(div5, p2);
			append_dev(p2, a0);
			append_dev(a0, t10);
			append_dev(p2, t11);
			append_dev(div5, t12);
			append_dev(div5, p3);
			append_dev(p3, t13);
			append_dev(p3, a1);
			append_dev(a1, t14);
			append_dev(p3, t15);
			append_dev(div5, t16);
			append_dev(div5, div4);
			append_dev(div4, div1);
			append_dev(div1, svg0);
			append_dev(svg0, path0);
			append_dev(div1, t17);
			append_dev(div1, span0);
			append_dev(span0, t18);
			append_dev(div4, t19);
			append_dev(div4, div2);
			append_dev(div2, svg1);
			append_dev(svg1, path1);
			append_dev(div2, t20);
			append_dev(div2, span1);
			append_dev(span1, t21);
			append_dev(div4, t22);
			append_dev(div4, div3);
			append_dev(div3, svg2);
			append_dev(svg2, path2);
			append_dev(div3, t23);
			append_dev(div3, span2);
			append_dev(span2, t24);
			insert_dev(target, t25, anchor);
			mount_component(footer, target, anchor);
			current = true;
		},
		p: noop,
		i: function intro(local) {
			if (current) return;
			transition_in(footer.$$.fragment, local);
			current = true;
		},
		o: function outro(local) {
			transition_out(footer.$$.fragment, local);
			current = false;
		},
		d: function destroy(detaching) {
			if (detaching) detach_dev(t0);
			if (detaching) detach_dev(div7);
			if (detaching) detach_dev(t25);
			destroy_component(footer, detaching);
		}
	};

	dispatch_dev("SvelteRegisterBlock", {
		block,
		id: create_fragment.name,
		type: "component",
		source: "",
		ctx
	});

	return block;
}

function instance($$self, $$props, $$invalidate) {
	let { $$slots: slots = {}, $$scope } = $$props;
	validate_slots("About", slots, []);
	const writable_props = [];

	Object.keys($$props).forEach(key => {
		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<About> was created with unknown prop '${key}'`);
	});

	$$self.$capture_state = () => ({ Footer });
	return [];
}

class About extends SvelteComponentDev {
	constructor(options) {
		super(options);
		init(this, options, instance, create_fragment, safe_not_equal, {});

		dispatch_dev("SvelteRegisterComponent", {
			component: this,
			tagName: "About",
			options,
			id: create_fragment.name
		});
	}
}

export default About;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJvdXQuZTA1MTdiODEuanMiLCJzb3VyY2VzIjpbXSwic291cmNlc0NvbnRlbnQiOltdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=
